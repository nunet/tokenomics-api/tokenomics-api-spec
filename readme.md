# Tokenomics API Spec

This repository contains the API Spec for the Tokenomics API, including sources, compiled pages as well as related diagrams and schematics.

## Source

NuNet Tokenomics API is described as Async API at [tokenomics-api-snippet.yaml](tokenomics-api-snippet.yaml)

## Generated API spec pages

We normally host three versions of the tokenomics-api, corresponding to the lifecycle of the platform:

- Stable (Main Branch) https://openapi.nunet.io/tokenomics-api-spec/main/asyncapi/
- Pre-release (Staging Branch) https://openapi.nunet.io/tokenomics-api-spec/staging/asyncapi/
- Cutting Edge (Develop Branch) https://openapi.nunet.io/tokenomics-api-spec/develop/asyncapi/

## Architecture

Here we keep the latest descriptions of the tokenomics api logic and architecture, specified in the spec file and exposed to specific blockchain in [https://gitlab.com/nunet/tokenomics-api](https://gitlab.com/nunet/tokenomics-api)

### Entity diagram

This shows how components interact in the system. Tokenomics API is only a part of all interactions between components, which explained more in full in the wiki on [ML on GPU use-case](https://gitlab.com/nunet/architecture/-/wikis/Sequence-diagram-for-GPU-ML-use-case) (see full sequence diagram).

<img
  src="./schematics/entity-diagram.mermaid.svg"
  alt="Entity diagram"
  width=70% />

Component implementations (at their current implementation level):
* [DMS (device-management-service)](https://gitlab.com/nunet/device-management-service)
* [ML WebApp](https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-webapp)
* [Management Daschboard](https://gitlab.com/nunet/management-dashboard)
* [Oracle](https://gitlab.com/nunet/oracle)
* [ML service container](https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service)

### Business logic diagram

<img
  src="./schematics/tokenomics-business-logic.mermaid.svg"
  alt="Business logic diagram"
  width=70% />

### Sequence diagrams

#### Job initiation sequence

<img
  src="./schematics/tokenomics-call-sequence-1.mermaid.svg"
  alt="Job initiation sequence"
  width=100% />

#### Job completion sequence

<img
  src="./schematics/tokenomics-call-sequence-2.mermaid.svg"
  alt="Job completion sequence"
  width=100% />

### Implementation links

Plutus is the native smart contract language for Cardano. It is a Turing-complete language written in Haskell, and Plutus smart contracts are effectively Haskell programs. The NuNet smart contract is stored at https://gitlab.com/nunet/tokenomics-api/tokenomics-api-cardano/-/blob/develop/src/SimpleEscrow.hs

Tasty framework was used to test the Plutus smart contract. The test file can be found at https://gitlab.com/nunet/tokenomics-api/tokenomics-api-cardano/-/blob/develop/test/Spec.hs


